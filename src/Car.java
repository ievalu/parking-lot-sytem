public class Car {
    private final String plateNumber;

    public Car(String plateNumber) {
        this.plateNumber = plateNumber.toLowerCase();
    }

    public String carType() {
        if (this.plateNumber.startsWith("car")) {
            return "car";
        }
        if (this.plateNumber.startsWith("ev")) {
            return "electric";
        }
        return "van";
    }

    public boolean hasValidPlateNumber() {
        return this.plateNumber.startsWith("car")
                || this.plateNumber.startsWith("ev")
                || this.plateNumber.startsWith("van");
    }
}
