public class Floor {
    private Integer freeSpaces;

    public Floor(Integer freeSpaces) {
        this.freeSpaces = freeSpaces;
    }

    public Integer getFreeSpaces() {
        return this.freeSpaces;
    }
}
