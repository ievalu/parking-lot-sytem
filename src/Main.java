import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ParkingLot parkingLot = new ParkingLot();
        launchSystem(parkingLot);
    }

    public static void launchSystem(ParkingLot parkingLot) {
        systemLoop: do {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Please choose one of the options: \n " +
                    "1. Enter the parking lot \n " +
                    "2. Settings \n " +
                    "3. Show all floors capacities \n " +
                    "4. Exit \n");
            String userInput = scanner.nextLine();
            switch (userInput) {
                case "1":
                    System.out.println("Enter the plate number");
                    String plateNumber = scanner.nextLine();
                    handleCarPlaceSearch(parkingLot, plateNumber);
                    break;
                case "2":
                    handleSettings(parkingLot);
                    break;
                case "3":
                    showCapacity(parkingLot);
                    break;
                case "4":
                    break systemLoop;
                default:
                    System.out.println("Please enter correct number");
                    break;
            }
        } while(true);
    }

    public static void handleCarPlaceSearch(ParkingLot parkingLot, String plateNumber) {
        Car car = new Car(plateNumber);
        if(car.hasValidPlateNumber()) {
            System.out.println("The nearest empty place is in the floor " + parkingLot.checkForSpace(car));
        }
        else {
            System.out.println("Invalid plate number");
        }
    }

    private static void handleSettings(ParkingLot parkingLot) {
        settingsLoop: do {
            System.out.print("Please choose what do you want to change:" +
                    "\n 1. Lowest floor (currently " + parkingLot.getLowestFloor() +
                    ")\n 2. Highest floor (currently " + parkingLot.getHighestFloor() +
                    ")\n 3. Floor capacity \n " +
                    "4. Floor to enter parking lot (currently " + parkingLot.getFloorToEnter() +
                    ")\n 5. Exit settings \n");
            Scanner scanner = new Scanner(System.in);
            String userInput = scanner.nextLine();
            switch (userInput) {
                case "1":
                    System.out.println("Enter the lowest possible floor, anything other than the number will cancel ");
                    String lowestFloor = scanner.nextLine();
                    if(isInteger(lowestFloor)){
                        int lowestFloorInt = Integer.parseInt(lowestFloor);
                        parkingLot.changeLowestFloor(lowestFloorInt);
                    }
                    break;
                case "2":
                    System.out.println("Enter the highest possible floor, anything other than the number will cancel ");
                    String highestFloor = scanner.nextLine();
                    if(isInteger(highestFloor)){
                        int highestFloorInt = Integer.parseInt(highestFloor);
                        parkingLot.changeHighestFloor(highestFloorInt);
                    }
                    break;
                case "3":
                    System.out.println("Enter the floor you want to change the capacity for, anything other than the number will cancel ");
                    String floor = scanner.nextLine();
                    if(isInteger(floor)){
                        int floorInt = Integer.parseInt(floor);
                        System.out.println("Enter the capacity");
                        String floorCapacity = scanner.nextLine();
                        if(isInteger(floorCapacity)){
                            int floorCapacityInt = Integer.parseInt(floorCapacity);
                            parkingLot.changeFloorCapacity(floorInt, floorCapacityInt);
                        }
                    }
                    break;
                case "4":
                    System.out.println("Enter the floor number you will be entering to parking lot from, " +
                            "it can be between " + parkingLot.getLowestFloor() +
                            " and " + parkingLot.getHighestFloor() + " , " +
                            "anything other than the number will cancel " +
                            parkingLot.getLowestFloor() + " to " + parkingLot.getHighestFloor());
                    String floorToEnter = scanner.nextLine();
                    if(isInteger(floorToEnter)){
                        int floorToEnterInt = Integer.parseInt(floorToEnter);
                        parkingLot.changefloorToEnter(floorToEnterInt);
                    }
                    break;
                case "5":
                    break settingsLoop;
                default:
                    System.out.println("Please enter correct number");
                    break;
            }
        } while(true);
    }

    public static void showCapacity(ParkingLot parkingLot) {
        for(int i = parkingLot.getLowestFloor(); i <= parkingLot.getHighestFloor(); i++) {
            System.out.println(i + ": " + parkingLot.getFloorCapacity(i));
        }
    }

    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        }
        catch(NumberFormatException e ) {
            return false;
        }
    }
}
