import java.util.HashMap;

public class ParkingLot {
    private int lowestFloor = -2;
    private int highestFloor = 2;
    private int floorToEnter = 0;
    private final int defaultFloorCapacity = 100;
    private HashMap<Integer, Floor> floorsDict = new HashMap<>();

    public int getLowestFloor() {
        return lowestFloor;
    }

    public int getHighestFloor() {
        return highestFloor;
    }

    public int getFloorToEnter() {
        return floorToEnter;
    }

    public int getFloorCapacity(int floor) {
        if (!floorsDict.containsKey(floor)) {
            return this.defaultFloorCapacity;
        }
        return floorsDict.get(floor).getFreeSpaces();
    }

    public void changeLowestFloor(int newLowestFloor) {
        if (newLowestFloor < this.highestFloor) {
            this.lowestFloor = newLowestFloor;
            System.out.println("Lowest floor changed to " + this.lowestFloor);
        } else {
            System.out.println("The lowest floor number must be smaller than highest floor number");
        }
    }

    public void changeHighestFloor(int newHighestFloor) {
        if (newHighestFloor > this.lowestFloor) {
            this.highestFloor = newHighestFloor;
            System.out.println("Highest floor changed to " + this.highestFloor);
        } else {
            System.out.println("The highest floor number must be bigger than lowest floor number");
        }
    }

    public void changeFloorCapacity(int floor, int newFloorCapacity) {
        if (floor <= this.highestFloor && floor >= this.lowestFloor) {
            this.floorsDict.put(floor, new Floor(newFloorCapacity));
            System.out.println("Floor capacity changed to " + newFloorCapacity);
        } else {
            System.out.println("Invalid floor number");
        }
    }

    public void changefloorToEnter(int newFloorToEnter) {
        if (newFloorToEnter >= this.lowestFloor && newFloorToEnter <= this.highestFloor) {
            this.floorToEnter = newFloorToEnter;
            System.out.println("Floor to enter changed to " + this.floorToEnter);
        } else {
            System.out.println("It should be between " + this.lowestFloor + " and " + this.highestFloor);
        }
    }

    public Integer checkForSpace(Car car) {
        String carType = car.carType();
        return assignFloor(carType);
    }

    private boolean isSpaceInFloor(int floorNumber) {
        return !floorsDict.containsKey(floorNumber) || floorsDict.get(floorNumber).getFreeSpaces() > 0;
    }

    private Integer assignFloor(String carType) {
        switch (carType) {
            case "car":
                for (int i = 0; i <= (this.highestFloor - this.lowestFloor + 1) / 2; i++) {
                    if (isSpaceInFloor(this.floorToEnter + i)) {
                        return this.floorToEnter + i;
                    }
                    if (isSpaceInFloor(this.floorToEnter - i)) {
                        return this.floorToEnter - i;
                    }
                }
                return null;
            case "electric":
                if (this.floorToEnter == this.highestFloor) {
                    for (int i = this.highestFloor; i >= this.highestFloor - 1; i--) {
                        if (isSpaceInFloor(i)) {
                            return i;
                        }
                    }
                } else {
                    for (int i = this.highestFloor - 1; i <= this.highestFloor; i++) {
                        if (isSpaceInFloor(i)) {
                            return i;
                        }
                    }
                }
                return null;
            case "van":
                if (this.floorToEnter == this.lowestFloor) {
                    for (int i = this.lowestFloor; i <= this.lowestFloor + 1; i++) {
                        if (isSpaceInFloor(i)) {
                            return i;
                        }
                    }
                } else {
                    for (int i = this.lowestFloor + 1; i >= this.lowestFloor; i--) {
                        if (isSpaceInFloor(i)) {
                            return i;
                        }
                    }
                }
                return null;
            default:
                return null;
        }
    }
}
